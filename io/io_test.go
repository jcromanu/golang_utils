package io

import "testing"

var resultString string

func TestFile(t *testing.T) {
	cases := []struct {
		input, expected string
	}{
		{"./testFiles/holamundo.txt", "hola mundo"},
		{"./testFiles/adiosmundo.txt", "adios mundo"},
	}
	for _, currentCase := range cases {
		resultString = FileToString(currentCase.input)
		if resultString != currentCase.expected {
			t.Errorf("TestFile expected %v got %v", currentCase.expected, resultString)
		}
	}
}
