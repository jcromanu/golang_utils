package io

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}

//FileToString reads content from file and returns a string
func FileToString(filePath string) string {
	data, error := ioutil.ReadFile(filePath)
	checkError(error)
	return strings.TrimSpace(string(data))
}

//StdoutPrint prints a string to standardoutput
func StdoutPrint(newString string) {
	fmt.Printf(newString)
}
